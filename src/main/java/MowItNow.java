import models.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public record MowItNow(Lawn lawn, List<Mower> mowers) {

    /**
     * Return a mowerStation from a string
     * String format :
     * First line : [lawn width] [lawn height]
     * Following lines : mower information
     * - mower information format :
     * [x] [y] [orientation]
     * [actions]
     *
     * @param input
     * @return
     */
    public static MowItNow fromLines(List<String> input) {
        final var lawn = readLawn(input);
        final var mowers = readMowers(input);
        return new MowItNow(lawn, mowers);
    }

    private static Lawn readLawn(List<String> input) {
        final String[] inputParts = input.get(0).split(" ");
        final var width = Integer.parseInt(inputParts[0]);
        final var height = Integer.parseInt(inputParts[1]);
        return new Lawn(width, height);
    }

    private static List<Mower> readMowers(List<String> input) {
        final var mowers = new ArrayList<Mower>();
        for (int i = 1; i < input.size(); i = i + 2) {
            final var initPosition = input.get(i).split(" ");
            final var actions = new ArrayList<Action>();
            for (Character character : input.get(i + 1).toCharArray()) {
                actions.add(Action.valueOf(character.toString()));
            }
            final var position = new Position(Integer.parseInt(initPosition[0]), Integer.parseInt(initPosition[1]));
            mowers.add(new Mower(
                    position,
                    Orientation.valueOf(initPosition[2]),
                    actions));
        }
        return mowers;
    }

    /**
     * Run all mowers
     *
     * @return moves result
     */
    public String runProgram() {
        final var output = new ArrayList<String>();
        for (Mower currentMower : mowers) {
            final var otherMowersPosition = mowers.stream()
                    .filter(mower -> !mower.equals(currentMower))
                    .map(Mower::getPosition)
                    .collect(Collectors.toList());
            final var mowerState = currentMower.mow(otherMowersPosition, lawn);
            output.add(String.format("%d %d %s", mowerState.position().x(), mowerState.position().y(), mowerState.orientation().name()));
        }
        return String.join(System.lineSeparator(), output);
    }
}
