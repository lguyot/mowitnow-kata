import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class MowKata {
    public static void main(String[] args) throws IOException {
        final var input = Files.readAllLines(Path.of(args[0]));

        final var mowItNow = MowItNow.fromLines(input);

        Files.writeString(Paths.get(args[1]), mowItNow.runProgram());
    }
}
