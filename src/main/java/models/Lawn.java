package models;

public record Lawn(int width, int height) {
    public boolean contains(Position position) {
        return position.x() <= width && position.x() >= 0 && position.y() <= height && position.y() >= 0;
    }
}
