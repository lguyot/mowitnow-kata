package models;

import java.util.List;
import java.util.Objects;

public class Mower {
    private Position position;
    private Orientation orientation;
    private final List<Action> actions;

    private final static List<Orientation> ORIENTATIONS = List.of(Orientation.N, Orientation.E, Orientation.S, Orientation.W);

    public Mower(Position initialPosition, Orientation orientation, List<Action> actions) {
        this.position = initialPosition;
        this.orientation = orientation;
        this.actions = actions;
    }

    public Position getPosition() {
        return position;
    }

    /**
     * Move the mower
     * @param forbiddenPositions
     * @param lawn
     * @return
     */
    public MowerState mow(List<Position> forbiddenPositions, Lawn lawn) {
        for (Action action : actions) {
            switch (action) {
                case A: {
                    final var newPosition = nextPosition();
                    if (lawn.contains(position) && !forbiddenPositions.contains(newPosition)) {
                        position = newPosition;
                    }
                    break;
                }
                case D: orientation = ORIENTATIONS.get((orientation.ordinal() + 1) % 4);
                    break;
                case G: orientation = ORIENTATIONS.get((orientation.ordinal() + 3) % 4);
                    break;
                default: throw new UnsupportedOperationException();
            }
        }
        actions.clear();
        return new MowerState(position, orientation);
    }

    private Position nextPosition() {
        return switch(orientation) {
            case E -> new Position(position.x() + 1, position.y());
            case S -> new Position(position.x(), position.y() - 1);
            case W -> new Position(position.x() - 1, position.y());
            case N -> new Position(position.x(), position.y() + 1);
            default -> throw new UnsupportedOperationException();
        };
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Mower mower)) return false;
        return Objects.equals(position, mower.position) && orientation == mower.orientation && Objects.equals(actions, mower.actions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(position, orientation, actions);
    }
}
