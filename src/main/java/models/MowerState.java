package models;

public record MowerState(Position position, Orientation orientation) {
}

