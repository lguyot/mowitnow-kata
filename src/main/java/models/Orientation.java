package models;

public enum Orientation {
    N, E, S, W
}
