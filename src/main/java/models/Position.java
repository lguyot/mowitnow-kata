package models;

public record Position(int x, int y) {
}
