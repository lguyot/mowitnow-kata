import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MowItNowTest {
    @Test
    @DisplayName("should work given string list input")
    public void shouldWork() {
        final var input = List.of(
                "5 5",
                "1 2 N",
                "GAGAGAGAA",
                "3 3 E",
                "AADAADADDA"
        );

        final var mowItNow = MowItNow.fromLines(input);
        final var result = mowItNow.runProgram();

        final var expectedResult = String.join(System.lineSeparator(), List.of(
                "1 3 N",
                "5 1 E"
        ));

        assertEquals(expectedResult, result);
    }

}