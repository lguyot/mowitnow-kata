package models;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class LawnTest {

    private static Stream<Arguments> provideValidPositionsAndLawnsForContains() {
        return Stream.of(
                Arguments.of(new Lawn(5,5), new Position(1,3)),
                Arguments.of(new Lawn(10,5), new Position(0,0)),
                Arguments.of(new Lawn(10,12), new Position(10,12))
        );
    }

    @ParameterizedTest
    @MethodSource("provideValidPositionsAndLawnsForContains")
    @DisplayName("should return true when supplied position is within bounds")
    public void shouldReturnTrueWhenSuppliedPositionIsWithinBounds(Lawn lawn, Position position) {
        final var actual = lawn.contains(position);

        assertTrue(actual);
    }

    private static Stream<Arguments> provideInvalidPositionsAndLawnsForContains() {
        return Stream.of(
                Arguments.of(new Lawn(5,5), new Position(-1,3)),
                Arguments.of(new Lawn(10,5), new Position(0,10)),
                Arguments.of(new Lawn(10,12), new Position(-5,15))
        );
    }

    @ParameterizedTest
    @MethodSource("provideInvalidPositionsAndLawnsForContains")
    @DisplayName("should return false when supplied position is out of bounds")
    public void shouldReturnFalseWhenSuppliedPositionIsOutOfBounds(Lawn lawn, Position position) {
        final var actual = lawn.contains(position);

        assertFalse(actual);
    }
}