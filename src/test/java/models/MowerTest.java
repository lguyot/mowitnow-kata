package models;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class MowerTest {
    private static Stream<Arguments> provideMowerWithValidMoveForMow() {
        return Stream.of(
                Arguments.of(
                        new Mower(new Position(2,3), Orientation.N, new ArrayList<>(List.of(Action.A))),
                        new Mower(new Position(2,4), Orientation.N,new ArrayList<>()),
                        new MowerState(new Position(2,4), Orientation.N)
                ),
                Arguments.of(
                        new Mower(new Position(1,4), Orientation.E, new ArrayList<>(List.of(Action.A))),
                        new Mower(new Position(2,4), Orientation.E, new ArrayList<>()),
                        new MowerState(new Position(2,4), Orientation.E)
                ),
                Arguments.of(
                        new Mower(new Position(3,3), Orientation.S, new ArrayList<>(List.of(Action.A))),
                        new Mower(new Position(3,2), Orientation.S, new ArrayList<>()),
                        new MowerState(new Position(3,2), Orientation.S)
                ),
                Arguments.of(
                        new Mower(new Position(2,5), Orientation.W, new ArrayList<>(List.of(Action.A))),
                        new Mower(new Position(1,5), Orientation.W, new ArrayList<>()),
                        new MowerState(new Position(1,5), Orientation.W)
                )
        );
    }

    @ParameterizedTest
    @MethodSource("provideMowerWithValidMoveForMow")
    @DisplayName("should move given lawn and forbidden positions")
    public void shouldMoveGivenLawnAndForbiddenPositions(Mower mower, Mower expectedMower, MowerState expectedMowerState) {
        final var lawn = new Lawn(5,5);
        final var forbiddenPositions = List.of(new Position(0, 0), new Position(5,5));

        final var actualMowerState = mower.mow(forbiddenPositions, lawn);

        assertEquals(expectedMower, mower);
        assertEquals(expectedMowerState, actualMowerState);
    }

    private static Stream<Arguments> provideMowerWithForbiddenMoveAndForbiddenPositionsForMow() {
        return Stream.of(
                Arguments.of(
                        List.of(new Position(2, 4)),
                        new Mower(new Position(2, 3), Orientation.N, new ArrayList<>(List.of(Action.A))),
                        new Mower(new Position(2, 3), Orientation.N, new ArrayList<>()),
                        new MowerState(new Position(2, 3), Orientation.N)
                ),
                Arguments.of(
                        List.of(new Position(2, 4)),
                        new Mower(new Position(1, 4), Orientation.E, new ArrayList<>(List.of(Action.A))),
                        new Mower(new Position(1, 4), Orientation.E, new ArrayList<>()),
                        new MowerState(new Position(1, 4), Orientation.E)
                ),
                Arguments.of(
                        List.of(new Position(3, 2)),
                        new Mower(new Position(3, 3), Orientation.S, new ArrayList<>(List.of(Action.A))),
                        new Mower(new Position(3, 3), Orientation.S, new ArrayList<>()),
                        new MowerState(new Position(3, 3), Orientation.S)
                ),
                Arguments.of(
                        List.of(new Position(1, 5)),
                        new Mower(new Position(2, 5), Orientation.W, new ArrayList<>(List.of(Action.A))),
                        new Mower(new Position(2, 5), Orientation.W, new ArrayList<>()),
                        new MowerState(new Position(2, 5), Orientation.W)
                )
        );
    }

    @ParameterizedTest
    @MethodSource("provideMowerWithForbiddenMoveAndForbiddenPositionsForMow")
    @DisplayName("should not move onto forbidden position given lawn and forbidden positions")
    public void shouldNotMoveOntoForbiddenPositionGivenLawnAndForbiddenPositions(List<Position> forbiddenPositions, Mower mower, Mower expectedMower, MowerState expectedMowerState) {
        final var lawn = new Lawn(5,5);

        final var actualMowerState = mower.mow(forbiddenPositions, lawn);

        assertEquals(expectedMower, mower);
        assertEquals(expectedMowerState, actualMowerState);
    }

    private static Stream<Arguments> provideMowerWithValidRotationsForMow() {
        return Stream.of(
                Arguments.of(
                        new Mower(new Position(2,4), Orientation.N, new ArrayList<>(List.of(Action.D))),
                        new Mower(new Position(2,4), Orientation.E,new ArrayList<>()),
                        new MowerState(new Position(2,4), Orientation.E)
                ),
                Arguments.of(
                        new Mower(new Position(2,4), Orientation.N, new ArrayList<>(List.of(Action.G))),
                        new Mower(new Position(2,4), Orientation.W,new ArrayList<>()),
                        new MowerState(new Position(2,4), Orientation.W)
                ),
                Arguments.of(
                        new Mower(new Position(2,4), Orientation.E, new ArrayList<>(List.of(Action.D))),
                        new Mower(new Position(2,4), Orientation.S, new ArrayList<>()),
                        new MowerState(new Position(2,4), Orientation.S)
                ),
                Arguments.of(
                        new Mower(new Position(2,4), Orientation.E, new ArrayList<>(List.of(Action.G))),
                        new Mower(new Position(2,4), Orientation.N, new ArrayList<>()),
                        new MowerState(new Position(2,4), Orientation.N)
                ),
                Arguments.of(
                        new Mower(new Position(3,2), Orientation.S, new ArrayList<>(List.of(Action.D))),
                        new Mower(new Position(3,2), Orientation.W, new ArrayList<>()),
                        new MowerState(new Position(3,2), Orientation.W)
                ),
                Arguments.of(
                        new Mower(new Position(3,2), Orientation.S, new ArrayList<>(List.of(Action.G))),
                        new Mower(new Position(3,2), Orientation.E, new ArrayList<>()),
                        new MowerState(new Position(3,2), Orientation.E)
                ),
                Arguments.of(
                        new Mower(new Position(1,5), Orientation.W, new ArrayList<>(List.of(Action.D))),
                        new Mower(new Position(1,5), Orientation.N, new ArrayList<>()),
                        new MowerState(new Position(1,5), Orientation.N)
                ),
                Arguments.of(
                        new Mower(new Position(1,5), Orientation.W, new ArrayList<>(List.of(Action.G))),
                        new Mower(new Position(1,5), Orientation.S, new ArrayList<>()),
                        new MowerState(new Position(1,5), Orientation.S)
                )
        );
    }

    @ParameterizedTest
    @MethodSource("provideMowerWithValidRotationsForMow")
    @DisplayName("should rotate given lawn and forbidden positions")
    public void shouldRotateGivenLawnAndForbiddenPositions(Mower mower, Mower expectedMower, MowerState expectedMowerState) {
        final var lawn = new Lawn(5,5);
        final var forbiddenPositions = List.of(new Position(0, 0), new Position(5,5));

        final var actualMowerState = mower.mow(forbiddenPositions, lawn);

        assertEquals(expectedMower, mower);
        assertEquals(expectedMowerState, actualMowerState);
    }

    private static Stream<Arguments> provideMowerWithMoveAndRotateAndForbiddenPositionsForMow() {
        return Stream.of(
                Arguments.of(
                        List.of(new Position(2, 4)),
                        new Mower(new Position(2, 3), Orientation.N, new ArrayList<>(List.of(Action.A, Action.D, Action.A))),
                        new Mower(new Position(3, 3), Orientation.E, new ArrayList<>()),
                        new MowerState(new Position(3, 3), Orientation.E)
                ),
                Arguments.of(
                        List.of(new Position(2, 4)),
                        new Mower(new Position(1, 4), Orientation.E, new ArrayList<>(List.of(Action.A, Action.D, Action.A))),
                        new Mower(new Position(1, 3), Orientation.S, new ArrayList<>()),
                        new MowerState(new Position(1, 3), Orientation.S)
                ),
                Arguments.of(
                        List.of(new Position(3, 2)),
                        new Mower(new Position(3, 3), Orientation.S, new ArrayList<>(List.of(Action.A, Action.D, Action.A))),
                        new Mower(new Position(2, 3), Orientation.W, new ArrayList<>()),
                        new MowerState(new Position(2, 3), Orientation.W)
                ),
                Arguments.of(
                        List.of(new Position(1, 4)),
                        new Mower(new Position(2, 4), Orientation.W, new ArrayList<>(List.of(Action.A, Action.D, Action.A))),
                        new Mower(new Position(2, 5), Orientation.N, new ArrayList<>()),
                        new MowerState(new Position(2, 5), Orientation.N)
                )
        );
    }

    @ParameterizedTest
    @MethodSource("provideMowerWithMoveAndRotateAndForbiddenPositionsForMow")
    @DisplayName("should move and rotate and deal with forbidden positions given lawn and forbidden positions")
    public void shouldMoveAndRotateAndDealWithForbiddenPositionsGivenLAwnAndForbiddenPositions(List<Position> forbiddenPositions, Mower mower, Mower expectedMower, MowerState expectedMowerState) {
        final var lawn = new Lawn(5,5);

        final var actualMowerState = mower.mow(forbiddenPositions, lawn);

        assertEquals(expectedMower, mower);
        assertEquals(expectedMowerState, actualMowerState);
    }

}